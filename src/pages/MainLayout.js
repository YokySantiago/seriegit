import { Component } from 'react'

import Header from './../components/Header'
import Footer from './../components/Footer'

class MainLayout extends Component {
  render() {
    return (
      <>
        <Header />
        {this.props.children}
        <Footer />
      </>
    )
  }
}

export default MainLayout
