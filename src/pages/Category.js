import { Component } from 'react'
import { Link } from 'react-router-dom'

import videosLogo from './../assets/images/undraw_video_files_fu10.svg'

import VideoCardMini from './../components/VideoCardMini'

class Category extends Component {
  state = {
    category_id: '',
    category: '',
    data: [],
    categories: {
      conceptos_basicos: 'Conceptos Básicos',
      colaborando: 'Colaborando',
      charlas: 'Charlas',
    },
  }

  componentDidMount = () => {
    const category_id = this.props.match.params.name
    this.changeData(category_id)
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.match.params.name !== prevProps.match.params.name) {
      const category_id = this.props.match.params.name
      this.changeData(category_id)
    }
  }

  changeData = (category_id) => {
    this.setState((state) => {
      return {
        ...state,
        category_id: category_id,
        category: state.categories[category_id],
        loading: true,
      }
    })

    import('./../data.json').then((data) => {
      let videos = data.data.filter(
        (item) => item.category_id === this.state.category_id
      )
      videos = videos.sort((itemA, itemB) => itemA.id - itemB.id)
      this.setState((state) => {
        return { ...state, data: videos, loading: false }
      })
    })
  }

  render() {
    return (
      <>
        <div className="container mx-auto px-4 py-8 flex flex-col justify-center items-center">
          <img
            src={videosLogo}
            alt="Vídeos logo"
            className="h-14 w-14 md:h-20 md:w-20"
          />
          <h3 className="text-2xl md:text-3xl text-red-500 font-bold uppercase -my-3">
            {this.state.category}
          </h3>
        </div>
        <div className="container mx-auto px-4 py-8 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
          {this.state.data.map((item) => (
            <Link to={`/video/${item.id}`} key={item.id}>
              <VideoCardMini
                video={item}
                loading={this.state.loading}
              ></VideoCardMini>
            </Link>
          ))}
        </div>
      </>
    )
  }
}

export default Category
