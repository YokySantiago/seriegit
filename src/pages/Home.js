import { Component } from 'react'
import { Link } from 'react-router-dom'

import videosLogo from './../assets/images/undraw_video_files_fu10.svg'

import VideoCard from './../components/VideoCard'
import VideoCardMini from './../components/VideoCardMini'

class Home extends Component {
  state = {
    data: [],
    lastVideo: {
      title: '',
      description: [],
      link: '',
      img: '',
    },
    loading: false,
  }

  componentDidMount = () => {
    this.setState((state) => {
      return { ...state, loading: true }
    })

    import('./../data.json').then((data) => {
      const lastVideo = data.data.find((item) => item.id === data.lastVideo)
      const videos = data.data.filter((item) => item.id !== data.lastVideo)
      this.setState((state) => {
        return { ...state, data: videos, lastVideo, loading: false }
      })
    })
  }

  render() {
    if (this.state.loading) {
      return <p>Cargando...</p>
    }
    return (
      <div className="py-4 grid">
        <div className="px-4 sm:w-10/12 sm:mx-auto">
          <VideoCard
            video={this.state.lastVideo}
            loading={this.state.loading}
          ></VideoCard>
        </div>
        <div className="container mx-auto px-4 py-8 flex flex-col justify-center items-center">
          <img
            src={videosLogo}
            alt="Vídeos logo"
            className="h-14 w-14 md:h-20 md:w-20"
          />
          <h3 className="text-2xl md:text-3xl text-red-500 font-bold uppercase -my-3">
            Videos <span className="text-gray-900">recientes</span>
          </h3>
        </div>
        <div className="container mx-auto px-4 py-8 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
          {this.state.data.map((item) => (
            <Link to={`/video/${item.id}`} key={item.id}>
              <VideoCardMini
                video={item}
                loading={this.state.loading}
              ></VideoCardMini>
            </Link>
          ))}
        </div>
      </div>
    )
  }
}

export default Home
