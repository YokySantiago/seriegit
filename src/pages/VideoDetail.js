import { Component } from 'react'
import { Link } from 'react-router-dom'

import videosLogo from './../assets/images/undraw_video_files_fu10.svg'

import VideoCard from './../components/VideoCard'
import VideoCardMini from './../components/VideoCardMini'

class VideoDetail extends Component {
  state = {
    loading: false,
    data: [],
    selectedVideo: {
      title: '',
      description: [],
      link: '',
      img: '',
    },
  }

  componentDidMount = () => {
    const videoId = this.props.match.params.id
    this.changeData(videoId)
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      const videoId = this.props.match.params.id
      this.changeData(videoId)
    }
  }

  changeData = (videoId) => {
    this.setState((state) => {
      return { ...state, loading: true }
    })

    import('./../data.json').then((data) => {
      const selectedVideo = data.data.find(
        (item) => parseInt(item.id) === parseInt(videoId)
      )
      const videos = data.data.filter((item) => item.id !== videoId)
      this.setState((state) => {
        return { ...state, data: videos, selectedVideo, loading: false }
      })
    })
  }

  render() {
    return (
      <>
        <div className="py-4 grid">
          <div className="px-4 sm:w-10/12 sm:mx-auto">
            <VideoCard
              video={this.state.selectedVideo}
              loading={this.state.loading}
            ></VideoCard>
          </div>
          <div className="container mx-auto px-4 py-8 flex flex-col justify-center items-center">
            <img
              src={videosLogo}
              alt="Vídeos logo"
              className="h-14 w-14 md:h-20 md:w-20"
            />
            <h3 className="text-2xl md:text-3xl text-red-500 font-bold uppercase -my-3">
              Videos <span className="text-gray-900">recientes</span>
            </h3>
          </div>
          <div className="container mx-auto px-4 py-8 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
            {this.state.data.map((item) => (
              <Link to={`/video/${item.id}`} key={item.id}>
                <VideoCardMini
                  video={item}
                  loading={this.state.loading}
                ></VideoCardMini>
              </Link>
            ))}
          </div>
        </div>
      </>
    )
  }
}

export default VideoDetail
