import { Component } from 'react'

import Video from './Video'

class VideoCardMini extends Component {
  render() {
    const video = this.props.video

    if (this.props.loading) {
      return (
        <>
          <div className="mb-2">
            <div className="w-auto h-40 min-h-full animate-pulse">
              <Video
                loading={this.props.loading}
                img={video.img}
                name={video.title}
                className="rounded-t-sm"
                onClick={this.onClick}
              ></Video>
            </div>
            <div className="h-6 w-full mt-4 bg-gray-300 rounded-sm"></div>
          </div>
        </>
      )
    }

    return (
      <>
        <div className="mb-2">
          <div className="w-auto h-40 min-h-full">
            <Video
              loading={this.props.loading}
              img={video.img}
              name={video.title}
              className="rounded-t-sm"
              onClick={this.onClick}
            ></Video>
          </div>
          <p className="mt-2 text-sm md:text-base text-blue-900">
            {video.title}
          </p>
        </div>
      </>
    )
  }
}

export default VideoCardMini
