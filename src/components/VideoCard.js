import { Component } from 'react'

import PlayIcon from './PlayIcon'
import Video from './Video'

class VideoCard extends Component {
  handleClickVideo = () => {
    window.open(this.props.video.link, '_blank')
  }

  render() {
    const video = this.props.video

    if (this.props.loading) {
      return (
        <>
          <div className="w-full relative shadow-xl grid grid-cols-1 xl:grid-cols-3">
            <div className="col-span-2">
              <Video
                loading={this.props.loading}
                img={video.img}
                name={video.title}
                className="rounded-t-sm"
                onClick={this.handleClickVideo}
              />
            </div>
            <div className="p-4 animate-pulse">
              <div className="h-6 mt-4 w-2/3 bg-gray-300 rounded-sm"></div>
              <div className="h-24 mt-4 w-full bg-gray-300 rounded-sm"></div>
              <div className="h-24 mt-4 w-full bg-gray-300 rounded-sm"></div>
              <div className="h-6 mt-4 w-1/3 bg-gray-300 rounded-sm"></div>
            </div>  
          </div>
        </>
      )
    }

    return (
      <>
        <div className="w-full relative shadow-xl grid grid-cols-1 xl:grid-cols-3">
          <div className="col-span-2">
            <Video
              loading={this.props.loading}
              img={video.img}
              name={video.title}
              className="rounded-t-sm"
              onClick={this.handleClickVideo}
            />
          </div>
          <div className="p-4 flex flex-col justify-center">
            <p className="text-white text-lg sm:text-xl md:text-2xl lg:text-3xl text-blue-400 mb-3">
              {video.title}
            </p>
            {video.description.map((item, index) => {
              return (
                <p className="text-sm mb-2" key={index}>
                  {item}
                </p>
              )
            })}
            <a
              href={video.link}
              target="_blank"
              rel="noreferrer"
              className="text-red-500 hover:underline flex mt-2 text-base"
            >
              <PlayIcon className="h-6 w-6"></PlayIcon> Ver vídeo
            </a>
          </div>
        </div>
      </>
    )
  }
}

export default VideoCard
