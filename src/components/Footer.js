import { Component } from 'react'

class Footer extends Component {
  state = {
    links: [
      {
        id: 'twitter',
        name: 'Twitter',
        to: 'https://twitter.com/YokySantiago',
      },
      {
        id: 'youtube',
        name: 'YouTube',
        to:
          'https://www.youtube.com/channel/UC6Jwk03Tx640N--pYQ3mU4A/?sub_confirmation=1',
      },
      { id: 'github', name: 'GitHub', to: 'https://github.com/YokySantiago' },
    ],
  }

  render() {
    return (
      <>
        <footer className="px-4 py-2 bg-gray-800 h-auto sm:h-14 flex flex-col sm:flex-row items-center justify-between">
          <a
            rel="noreferrer"
            target="_blank"
            href="https://www.youtube.com/watch?v=VSD5PRyi0Ig&list=PL5F7r4iT-6onL96B88nfmUiyW1q4NP_Ki"
          >
            <div className="text-red-500 font-bold text-xl px-4">
              <span>Curso Git YouTube</span>
            </div>
          </a>
          <div className="flex flex-col sm:flex-row items-center h-full">
            <ul className="flex flex-col sm:flex-row items-center text-white">
              {this.state.links.map((link) => (
                <li className="ml-4 font-light" key={link.id}>
                  <a href={link.to} rel="noreferrer" target="_blank">
                    {link.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </footer>
      </>
    )
  }
}

export default Footer
