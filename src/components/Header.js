import { Component } from 'react'
import { Link } from 'react-router-dom'

class Header extends Component {
  state = {
    links: [
      { name: 'Charlas', to: 'charlas' },
      { name: 'Conceptos Básicos', to: 'conceptos_basicos' },
      { name: 'Colaborando', to: 'colaborando' },
    ],
  }
  render() {
    return (
      <>
        <header className="px-4 py-2 bg-gray-800 h-auto sm:h-14 flex flex-col sm:flex-row items-center justify-between">
          <Link to={`/`}>
            <div className="text-white font-bold text-xl px-4">
              <span>YokyCode</span>
            </div>
          </Link>
          <div className="flex flex-col sm:flex-row items-center h-full">
            <ul className="flex flex-col sm:flex-row items-center text-blue-400">
              {this.state.links.map((link) => (
                <li className="ml-3 font-light" key={link.to}>
                  <Link to={`/category/${link.to}`}>{link.name}</Link>
                </li>
              ))}
            </ul>
          </div>
        </header>
      </>
    )
  }
}

export default Header
