import { Component } from 'react'

import PlayIcon from './PlayIcon'

class Video extends Component {
  render() {
    if (this.props.loading) {
      return (
        <div className="animate-pulse w-full h-full relative bg-gray-300"></div>
      )
    }
    return (
      <div className="w-full h-full relative">
        <img src={this.props.img} alt="Main vídeo" />
        <div className="absolute bg-gray-900 bg-opacity-10 inset-0">
          <div
            className="flex items-center justify-center h-full w-full text-white hover:text-red-500 cursor-pointer"
            onClick={this.props.onClick}
          >
            <PlayIcon className="h-24 w-24"></PlayIcon>
          </div>
        </div>
      </div>
    )
  }
}

export default Video
