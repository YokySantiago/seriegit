import React, { Suspense, lazy } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import MainLayout from './pages/MainLayout'

const Home = lazy(() => import('./pages/Home'))
const Category = lazy(() => import('./pages/Category'))
const VideoDetail = lazy(() => import('./pages/VideoDetail'))

function App() {
  return (
    <div className="App font-roboto text-gray-900">
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <MainLayout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/category/:name" component={Category} />
              <Route exact path="/video/:id" component={VideoDetail} />
            </Switch>
          </MainLayout>
        </Suspense>
      </Router>
    </div>
  )
}

export default App
